var app =  app || {};
app.utils = {
    'getFormValues' : function (selector, context) {
        var formData = {},
            $modelFields = context.$el.children(selector).children().not("label, button");
        $modelFields.each(function (i, el) {
            if ($(el).val() !== "") {
                formData[el.id] = $(el).val();
            }
        });
        return formData;
    }
};