
(function ($) {
    "use strict";
    $(document).foundation();
    var movieGroupView = new app.AllMoviesView(),
        actorGroupView = new app.AllActorsView(),
        movieDbRouter = new app.Router();
    Backbone.history.start();

    
})(jQuery);
