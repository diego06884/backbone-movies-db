

var app = app || {};
app.SingleMovie = Backbone.Model.extend({
  
  defaults : {
    name: "Movie Default Title",
    releaseYear : "1900",
    grossIncome : 0,
    cast : {},
    directorName : "Default Director Name",
    genre : "n/a"
  },
  initialize: function(){
    console.log('a movie was created');
    var _this = this;
    _this.on('change', function(){
      console.log('something has changed');
    });
  }
});