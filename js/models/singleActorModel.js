
    var app = app || {};
    app.SingleActor = Backbone.Model.extend({

      defaults : {
        firstName: "John",
        lastName : "Smith",
        gender : "male",
        birthDate : new Date(1900, 01, 01),
        filmography : {}
      }

    });
