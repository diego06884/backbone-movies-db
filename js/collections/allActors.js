var app = app || {};
app.ActorsCollection = Backbone.Collection.extend({
    localStorage: new Backbone.LocalStorage("actors-db"),
    model : app.SingleActor,
    comparator: function (actor){
        return -actor.get('lastName');
    }
});