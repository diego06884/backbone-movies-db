

var app = app || {};

app.MoviesCollection = Backbone.Collection.extend({
    localStorage : new Backbone.LocalStorage("movies -storage"),
//    parse: function (response) {
//    },
    model : app.SingleMovie,
    comparator : function (movie) {
        return -movie.get('releaseYear');
    }
});
