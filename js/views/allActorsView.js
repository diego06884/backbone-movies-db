var app = app ||{};
app.AllActorsView = Backbone.View.extend({
    el: $("#allActors"),
    initialize: function (){
        //instanciate a collection for the view
        this.collection =  new app.ActorsCollection();
        
        //populate collection
        this.collection.fetch();
        this.render(); //set up self rendering View
        
        //react to collection events
        this.collection.on("reset", this.render, this);
        this.collection.on("add", this.renderActor, this);
        this.collection.on("delete", this.removeActor, this);
    },
    events: {
        "click #addNewActor" : "addNewActor"
    },
    render: function (){
        this.collection.each(this.renderActor, this);
        return this;
    },
    renderActor: function(actor){
        var actorView =  new app.SingleActorView({
            model: actor
        });
        this.$el.append(actorView.render().el);
    },
    addNewActor: function (e){
        console.log('adding new actor');
        e.preventDefault();
        var formData = {}; 
        //grab the form state and create an object with it
        $("#addActor").children('input').each(function(i, el){
            if($(el).val() != ""){
                formData[el.id] = $(el).val();
            }
        });    
        //add it to the collection
        this.collection.create(formData); 
    },
    removeActor: function (removedActor){
        console.log('remove actor');
    }
    

});