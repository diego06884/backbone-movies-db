var app  =  app || {};

app.SingleMovieView =  Backbone.View.extend({

  tagName: 'article',
  className: 'movieListItem',

  template: _.template($("#movieElement").html()),

  initialize : function (){
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);    
  },
    
  render: function(){
    var movieTemplate = this.template(this.model.toJSON());
    this.$el.html(movieTemplate);
    return this;
  },
    
  events : {
        "click .delete" : "deleteMovie",
        "click .edit": "editMovie",
        "click .save-movie" : "updateMovie"  
  }, 
    
  deleteMovie:function () {
    this.model.destroy();
      this.remove(); // this is probably not necessary since I'm listening and removing the view on model destroy event.
  },
    
  editMovie: function(){
    this.$el.addClass("editing");
  },
  updateMovie: function(e) { 
      e.preventDefault();   
      var formData = app.utils.getFormValues('.edit-movie', this);
      this.model.save(formData);
      this.$el.removeClass("editing");
  }
      
  
});