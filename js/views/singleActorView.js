var app = app || {};

app.SingleActorView = Backbone.View.extend({
    tagName: 'article',
    className: 'actor-list-item',
    template: _.template($('#actorTemplate').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },
    render: function () {
        var actorTemplate = this.template(this.model.toJSON());
        this.$el.html(actorTemplate);
        return this;
    },
    events: {
        "click .delete-actor": "deleteActor",
        "click .edit-actor-info": "editActor",
        "click .save-actor": "updateActor"
    },
    deleteActor: function () {
        this.model.destroy();
        this.remove(); // this is probably not necessary since I'm listening and removing the view on model destroy event.
    },
    editActor: function () {
        this.$el.addClass("editing");
    },
    updateActor: function (e) {
        console.log('updating actor');
        e.preventDefault();
        var formData = app.utils.getFormValues('.edit-actor', this);
        this.model.save(formData);
        this.$el.removeClass("editing");
    }

});