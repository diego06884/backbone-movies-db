
var app = app || {};

app.AllMoviesView = Backbone.View.extend({
    el: $("#allMovies"),
    initialize: function() {
        //this.collection = new app.MoviesCollection(movies); //initialize movies
        this.collection = new app.MoviesCollection();
        this.collection.fetch();
        this.render(); //self rendered view
        this.collection.on("add", this.renderMovie, this);
        this.collection.on("reset", this.render, this);
    },
    render: function() {
        this.collection.each(this.renderMovie, this);
        return this;
    },
    events: {
        "click #addNewMovie" : "addNewMovie"
     },

    addNewMovie: function(e){
        e.preventDefault();     
        var formData = {};

        $("#addMovie").children("input").each(function(i, el){
            if($(el).val() !== ""){
                formData[el.id] = $(el).val();
            }
            
        });
        this.collection.create(formData);
    },  

    renderMovie: function(movie) {
        var movieView = new app.SingleMovieView({
            model: movie
        });
        this.$el.append(movieView.render().el);
    }
});